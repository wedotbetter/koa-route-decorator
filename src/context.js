import createRouteDecorator from './utils/createRouteDecorator';

export const Context = routes => context => createRouteDecorator(routes)(() => ({
	context
}));
