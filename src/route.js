import _ from 'lodash';
import pathToRegexp   from 'path-to-regexp';

import createRouteDecorator from './utils/createRouteDecorator';

export const Route = routes => (method, path = '') => createRouteDecorator(routes)(() => ({
	path,
	method
}));

export const Directory = routes => path => createRouteDecorator(routes)(() => ({
	path
}));

export const GET    = routes => path => Route(routes)('GET', path);
export const POST   = routes => path => Route(routes)('POST', path);
export const PUT    = routes => path => Route(routes)('PUT', path);
export const DELETE = routes => path => Route(routes)('DELETE', path);
export const PATCH  = routes => path => Route(routes)('PATCH', path);
export const isRoute = target => {
	target.isRoute = true;
	return target;
};

export const checkRoute = (ctx, method, path) => {
	let keys = pathToRegexp.parse(path);
	let matches = [];
	if (path)
		matches = pathToRegexp(path, keys).exec(ctx.path);
	if ((!method || method == ctx.method) && matches)
		return _.reduce(matches, (result, match, i) => {
			if (_.isObject(keys[i]))
				return { ...result, [keys[i].name] : match };
			return result;
		}, {});
	return null;
};
