import createRouteDecorator from './utils/createRouteDecorator';

export const Redirect = routes => (...redirect) => createRouteDecorator(routes)(() => ({
	redirect
}));
