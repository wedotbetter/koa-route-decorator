import sinon from 'sinon';

export default function spy(target, name, descriptor) {
	let orig = descriptor.initializer ? descriptor.initializer() : descriptor.value;
	if (!process.env.ENABLE_SPY)
		return descriptor;
	let spy = sinon.spy();
	let value = function (...args) {
		if (!value.enableSpy)
			return orig.call(target, ...args);
		return spy(...args);
	};
	value.spy = spy;
	return {
		...descriptor,
		value
	};
}