import debug from 'debug';

export const debugRoute = debug('koa-route-decorator:route');
export const debugRuntime = debug('koa-route-decorator:runtime');
export const debugRuntimeRoute = debug('koa-route-decorator:runtime:route');
export const debugRuntimeCriteria = debug('koa-route-decorator:runtime:criteria');
