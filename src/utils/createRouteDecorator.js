import uuid   from 'uuid';
import _ from 'lodash';
import { debugRoute } from './debug';

import { id } from '../constants';


export default routes => decorator => (target, key, descriptor) => {

	let mid = uuid.v4();

	if (target.isRoute && !_.isString(key)) {
		target[id] = target[id] || uuid.v4();
		let routeIndex = _.findIndex(routes, { target : target[id] });
		if (~routeIndex) {
			routes[routeIndex] = {
				...routes[routeIndex],
				...decorator(target)
			};
		}
		routes.push({
			target : target[id],
			key,
			controller : target,
			...decorator(target, null, null, null, mid)
		});
		return target;
	}

	const setDefault = target => {
		let keys = Object.getOwnPropertyNames(target);


		_.forEach(keys, key => {
			if (_.isObject(target[key]) && target[key][id])
				setDefault(target[key]);
			let routeIndex = _.findIndex(routes, { target : target[id], key });
			if (~routeIndex) {
				let directory = decorator(target, key, Object.getOwnPropertyDescriptor(target, key), routes[routeIndex], mid);
				if (_.some(directory, (item, key) => !~_.indexOf(['method', 'path', 'middlewares', 'criteria'], key))) {
					debugRoute('Route and method decorators are not applicable to directory target');
					return;
				}
				let { criteria, path = '', middlewares } = directory;
				let route = routes[routeIndex];

				routes[routeIndex] = {
					...directory,
					...route,
					path : path + route.path,
					directories : [
						..._.map(
							route.directories,
							directory => ({
								...directory,
								path : path + directory.path.replace(/\/+/g, '/').replace(/(\/:PATH\*)$/, '') + '/:PATH*'
							})
						), {
							criteria,
							middlewares,
							path : path.replace(/\/+/g, '/').replace(/(\/:PATH\*)$/, '') + '/:PATH*'
						}
					]
				};
			}
		});
	};

	// Decorate Classes or Object literals
	if (!_.isString(key)) {
		setDefault(target);
		return target;
	}

	target[id] = target[id] || uuid.v4();

	// Decorate Methods
	let routeIndex = _.findIndex(routes, { target : target[id], key });
	if (~routeIndex) {
		routes[routeIndex] = {
			controller : descriptor.initializer ? descriptor.initializer() : descriptor.value,
			...routes[routeIndex],
			...decorator(target, key, descriptor, routes[routeIndex], mid)
		};
	} else {
		routes.push({
			target : target[id],
			key,
			context : target,
			controller : descriptor.initializer ? descriptor.initializer() : descriptor.value,
			...decorator(target, key, descriptor, mid)
		});
	}

	return descriptor;
};
