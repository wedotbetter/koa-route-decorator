import _ from 'lodash';

import createRouteDecorator from './utils/createRouteDecorator';
import { debugRoute } from './utils/debug';

export const Criteria = routes => (...criteria) => createRouteDecorator(routes)((target, key, descripter, route = {}) => {
	if ('$or' in criteria && _.keys(criteria).length != 1) {
		debugRoute('$or, if present, must be the only key in the criteria description');
		return route;
	}
	if ('$or' in criteria && !_.isArray(criteria['$or'])) {
		debugRoute('$or must be present in the format of array');
		return route;
	}
	return {
		criteria : [...(route.criteria || []), criteria]
	};
});

export const checkCriteria = (ctx, criteria) => {
	let result = true;
	if (_.isArray(criteria))
		result = _.every(criteria, criterion => checkCriteria(ctx, criterion));
	if (_.isFunction(criteria))
		result = criteria(ctx);
	if (_.isRegExp(criteria))
		result = criteria.test(ctx.path);
	if (_.isObject(criteria) && '$or' in criteria && _.isArray(criteria['$or']) && _.keys(criteria).length == 1)
		result = _.some(criteria['$or'], criterion => checkCriteria(ctx, criterion));
	if (_.isString(criteria))
		result = new RegExp(criteria).test(ctx.path);
	return result;
};
