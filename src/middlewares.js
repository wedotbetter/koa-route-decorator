import createRouteDecorator from './utils/createRouteDecorator';
import compose from 'koa-compose';

const memorize = (fn, mid) => (forceRun, memory) => (ctx, next) => {
	if (forceRun || !memory[mid]) {
		memory[mid] = true;
		return fn(ctx, next);
	}
	return next();
};

export const Middleware = routes => (...middlewares) => createRouteDecorator(routes)((t, k, d, r, mid) => ({
	middlewares : memorize(compose(middlewares), mid)
}));
