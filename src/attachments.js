import createRouteDecorator from './utils/createRouteDecorator';

export const Attachments = routes => (...attachments) => createRouteDecorator(routes)(() => ({
	attachments
}));
