import _ from 'lodash';

import createRouteDecorator from './utils/createRouteDecorator';

export const Response = routes => (...response) => createRouteDecorator(routes)(() => ({
	response
}));

export const setResponse = (ctx, response, result) => {
	if (response.length == 2) {
		let [ from , to ] = response;
		if (!_.isArray(to))
			to = to.split('.');
		_.set(ctx, ['body', ...to], _.get(result, from));
	}

	if (response.length == 1) {
		if (_.isFunction(response[0])) {
			let [ responseFn ] = response;
			responseFn(ctx, result);
		} else {
			let [ to ] = response;
			if (!_.isArray(to))
				to = to.split('.');
			_.set(ctx, ['body', ...to], result);
		}
	}

	if (response.length == 0) {
		ctx.body = result;
	}
};
