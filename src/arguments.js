import _ from 'lodash';

import createRouteDecorator from './utils/createRouteDecorator';

export const CONST = class {
	constructor(v) {
		this.v = v;
	}
};

export const Arguments = routes => (...args) => createRouteDecorator(routes)(() => ({
	args
}));

export const parseArgument = (ctx, arg) => {
	if (arg instanceof CONST)
		return arg.v;
	if (_.isArray(arg))
		return _.map(arg, item => parseArgument(ctx, item));
	if (_.isFunction(arg))
		return arg(ctx);
	if(_.isObject(arg))
		return _.reduce(arg, (result, item, key) => ({
			...result,
			[key] : parseArgument(ctx, item)
		}), {});
	if (_.isString(arg))
		return _.get(ctx, arg);
	return arg;
};
