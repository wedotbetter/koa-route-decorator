import compose from 'koa-compose';
import _ from 'lodash';

import { Arguments, CONST, parseArgument } from './arguments';
import { Route, Directory, GET, POST, PUT, DELETE, PATCH, isRoute, checkRoute } from './route';
import { Context } from './context';
import { Criteria, checkCriteria } from './criteria';
import { Response, setResponse } from './response';
import { Middleware } from './middlewares';
import { Attachment } from './attachments';
import { Redirect } from './redirect';
import { debugRoute, debugRuntimeRoute, debugRuntimeCriteria } from './utils/debug';

const defaultRoutes = [];

const enumerable = (target, key, descriptor) => {
	return {
		...descriptor,
		enumerable : true
	};
};

export default class Router {
	constructor(routes = defaultRoutes) {
		this.routes = routes;
	}

	@enumerable
	get Middleware() {
		return Middleware(this.routes);
	}

	@enumerable
	get Context() {
		return Context(this.routes);
	}

	@enumerable
	get Arguments() {
		return Arguments(this.routes);
	}

	@enumerable
	get CONST() {
		return CONST;
	}

	@enumerable
	get isRoute() {
		return isRoute;
	}

	@enumerable
	get Route() {
		return Route(this.routes);
	}

	@enumerable
	get Directory() {
		return Directory(this.routes);
	}

	@enumerable
	get GET() {
		return GET(this.routes);
	}

	@enumerable
	get POST() {
		return POST(this.routes);
	}

	@enumerable
	get PUT() {
		return PUT(this.routes);
	}

	@enumerable
	get DELETE() {
		return DELETE(this.routes);
	}

	@enumerable
	get PATCH() {
		return PATCH(this.routes);
	}

	@enumerable
	get Criteria() {
		return Criteria(this.routes);
	}

	@enumerable
	get Response() {
		return Response(this.routes);
	}

	@enumerable
	get Attachment() {
		return Attachment(this.routes);
	}

	@enumerable
	get Redirect() {
		return Redirect(this.routes);
	}

	_createMiddleware({ method, path, criteria, middlewares }, handler) {
		return async (ctx, next) => {
			ctx.state.__cache__ = ctx.state.__cache__ || {};
			let params = checkRoute(ctx, method, path);
			debugRuntimeRoute('----------------------------------------');
			debugRuntimeRoute({ method, path, criteria, middlewares });
			debugRuntimeRoute(params);
			if (!params)
				return next();
			ctx.state.params = { ...ctx.state.params, ...params };
			let fulfill = await Promise.resolve(checkCriteria(ctx, criteria));
			debugRuntimeCriteria('----------------------------------------');
			debugRuntimeCriteria({ method, path, criteria, middlewares });
			debugRuntimeCriteria(fulfill);
			if (criteria && !fulfill)
				return next();
			return compose([ ...(middlewares ? [middlewares(false, ctx.state.__cache__)] : []), handler ])(ctx, next);
		};
	}

	build() {
		debugRoute(this.routes);
		return compose(_.map(this.routes, route => {
			let {
				directories,
				args = [],
				redirect,
				attachments,
				controller = function() {},
				response,
				context,
				...routeDescription
			} = route;
			let handler = async (ctx, next) => {
				let parsedArgs = args.length ? _.map(args, arg => parseArgument(ctx, arg)) : [ctx, next];
				let result = await Promise.resolve(controller.call(context, ...parsedArgs));
				if (response) {
					setResponse(ctx, response, result);
					if (redirect)
						ctx.redirect(...redirect);
					if (attachments)
						ctx.attachments(attachments);
					return next();
				}
			};

			return _.reduce(directories, (resultMiddleware, directory) => {
				return this._createMiddleware(directory, resultMiddleware);
			}, this._createMiddleware(routeDescription, handler));
		}));
	}
}
