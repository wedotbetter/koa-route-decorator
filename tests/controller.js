import _ from 'lodash';

import router from './router';

let { Route, GET, Criteria, Middleware, Directory, Arguments, CONST, isRoute, Response } = router;

const users = [{
	_id : 1,
	username : 'userOne',
	deleted : false,
}, {
	_id : 2,
	username : 'userTwo',
	deleted : true,
}];

const projects = [{
	_id : 1,
	name : 'projectOne',
	owner : 'userOne',
	deleted : false,
}, {
	_id : 2,
	name : 'projectTwo',
	owner : 'userTwo',
	deleted : true,
}];

const authenticate = (ctx, next) => {
	ctx.state.authenticated = true;
	return next();
};

Middleware(authenticate)(
Directory('/api')(
	{
		versionOne : Directory('/v1/users/:username')(
			{
				@Route('GET')
				@Criteria(ctx => ctx.state.authenticated)
				@Arguments('state.params.username', 'query.deleted', new CONST('test'))
				@Response('data')
				getUsers : (username, deleted = false) => {
					return _.find(users, { username, deleted });
				}
			}
		),

		@GET('/users/:username')
		@Criteria(ctx => ctx.state.authenticated)
		@Arguments('state.params.username', 'query.deleted', new CONST('test'))
		getUsers : (username, deleted = false) => {
			return _.filter(users, { username, deleted });
		}
	}
));

const middlewareOrder = order => (ctx, next) => {
	ctx.state.order = ctx.state.order || [];
	ctx.state.order.push(order);
	return next();
};

@Directory('/v1')
@Middleware(middlewareOrder(2))
class SubRoute {
	@GET('/order')
	@Middleware(middlewareOrder(3))
	@Arguments('state.order')
	@Response('order')
	static async getOrder(order) {
		return order;
	}
}

@Directory('/api')
@Middleware(middlewareOrder(1))
export class Controller {
	@GET('/projects')
	@Arguments('query.deleted')
	@Response('data')
	static async listProjects(deleted = false) {
		return await Promise.resolve(_.filter(projects, { deleted }));
	}

	@GET('/projects/deleted')
	@Response('data')
	static async listDeletedProjects() {
		return await this.listProjects(true);
	}

	static SubRoute = SubRoute
}



GET('/api/users/:username/projects')(
	isRoute(
		async function listUserProjects(ctx, next) {
			ctx.body = { data : _.filter(projects, { owner : _.find(users, { username : ctx.query.username }) }) };
			return next();
		}
	)
);
