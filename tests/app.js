import Koa from 'koa';

import './controller';
import router from './router';

let koa = new Koa();
koa.use(router.build());

export default koa.listen(3000);
