import { expect } from 'chai';

import { Arguments, parseArgument, CONST } from '../src/arguments';
import { id } from '../src/constants';

describe('Arguments decorator', () => {
	it('should expose Arguments, parseArgument, CONST', () => {
		expect(Arguments).to.be.a('function');
		expect(parseArgument).to.be.a('function');
		expect(CONST).to.be.a('function');
	});
	describe('Arguments', () => {
		let routes = [];
		let ArgumentsDecorator = Arguments(routes);
		let controller = function() {};
		ArgumentsDecorator('testing')({ [id] : 'id' }, 'key', { value : controller });
		it('should take routes and return a function that forward arguments description to route', () => {
			expect(ArgumentsDecorator).to.be.a('function');
			expect(routes).to.deep.eql([{
				context : { [id] : 'id' },
				key : 'key',
				target : 'id',
				controller,
				args : ['testing']
			}]);
		});
	});
	describe('parseArgument', () => {
		let ctx = {
			state : {
				test : 'string'
			},
			protocol : 'function'
		};
		let stringArgument = 'state.test';
		let functionArgument = ctx => ctx.protocol;
		let constantArgument = new CONST('constant');
		let arrayArguments = [stringArgument, functionArgument, constantArgument];
		let objectArgument = {
			string : stringArgument,
			function : functionArgument,
			constant : constantArgument,
			array : arrayArguments
		};
		describe('with string argument', () => {
			it('should extract the target value specified by the string', () => {
				expect(parseArgument(ctx, stringArgument)).to.eql('string');
			});
		});
		describe('with fucntion argument', () => {
			it('should extract the target value specified by the string', () => {
				expect(parseArgument(ctx, functionArgument)).to.eql('function');
			});
		});
		describe('with constant argument', () => {
			it('should extract the target value specified by the string', () => {
				expect(parseArgument(ctx, constantArgument)).to.eql('constant');
			});
		});
		describe('with array argument', () => {
			it('should extract the target value specified by the string', () => {
				expect(parseArgument(ctx, arrayArguments)).to.deep.eql([ 'string', 'function', 'constant' ]);
			});
		});
		describe('with object argument', () => {
			it('should extract the target value specified by the string', () => {
				expect(parseArgument(ctx, objectArgument)).to.deep.eql({
					string : 'string',
					function : 'function',
					constant : 'constant',
					array : [ 'string', 'function', 'constant' ]
				});
			});
		});
	});
});
