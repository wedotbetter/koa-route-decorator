import chai, { expect } from 'chai';
import chaiSubset from 'chai-subset';
chai.use(chaiSubset);

import { Middleware } from '../src/middlewares';
import { id } from '../src/constants';

describe('Middleware decorator', () => {
	it('should expose Middleware', () => {
		expect(Middleware).to.be.a('function');
	});
	describe('Middleware', () => {
		let routes = [];
		let MiddlewareDecorator = Middleware(routes);
		let controller = function() {};
		let middleware = function(ctx, next) { return next(); };
		MiddlewareDecorator(middleware)({ [id] : 'id' }, 'key', { value : controller });
		it('should take routes and return a function that forward middlewares to route', () => {
			expect(MiddlewareDecorator).to.be.a('function');
			expect(routes[0]).to.containSubset({
				context : { [id] : 'id' },
				key : 'key',
				target : 'id',
				controller
			});
			expect(routes[0].middlewares).to.be.a('function');
		});
	});

});
