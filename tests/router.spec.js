import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import _ from 'lodash';

import router from './router';
import app from './app';

chai.use(chaiHttp);

describe('koa-route-decorator\'s', () => {
	it('should expose Router class', () => {
		expect(router).to.be.an('object');
	});
	describe('Router', () => {
		it('should expose decorators as enumerable properties', () => {
			let decorators = ['Route', 'isRoute', 'Directory', 'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'Arguments', 'CONST', 'Criteria', 'Response', 'Middleware', 'Context'];
			_.forEach(decorators, decorator => {
				expect(router[decorator]).to.be.a('function');
			});
		});
		let middleware;
		it('should able to build koa middleware with routes decription', () => {
			middleware = router.build();
			expect(middleware).to.be.a('function');
		});
	});
});

describe('Usage test', () => {
	describe('GET /api/v1/users/userOne', () => {
		it('it should GET userOne', (done) => {
			chai.request(app)
				.get('/api/v1/users/userOne')
				.end((err, res) => {
					expect(res).have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body.data).to.eql({
						_id : 1,
						username : 'userOne',
						deleted : false,
					});
					done();
				});
		});
	});

	describe('GET /api/projects/deleted', () => {
		it('it should GET deleted projects', (done) => {
			chai.request(app)
				.get('/api/projects/deleted')
				.end((err, res) => {
					expect(res).have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body.data).to.eql([{
						_id : 2,
						name : 'projectTwo',
						owner : 'userTwo',
						deleted : true,
					}]);
					done();
				});
		});
	});

	describe('GET /api/v1/order', () => {
		it('it should GET middleware order as an array', (done) => {
			chai.request(app)
				.get('/api/v1/order')
				.end((err, res) => {
					expect(res).have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body.order).to.eql([1, 2, 3]);
					done();
				});
		});
	});
});
