import { expect } from 'chai';

import { Response, setResponse } from '../src/response';
import { id } from '../src/constants';

describe('Response decorator', () => {
	it('should expose Response and setResponse', () => {
		expect(Response).to.be.a('function');
		expect(setResponse).to.be.an('function');
	});
	describe('Response', () => {
		let routes = [];
		let ResponseDecorator = Response(routes);
		let controller = function() {};
		ResponseDecorator('testing')({ [id] : 'id' }, 'key', { value : controller });
		it('should take routes and return a function that forward reponse description to route', () => {
			expect(ResponseDecorator).to.be.a('function');
			expect(routes).to.deep.eql([{
				context : { [id] : 'id' },
				key : 'key',
				target : 'id',
				controller,
				response : ['testing']
			}]);
		});
	});
	describe('setResponse', () => {
		describe('with no arguments', () => {
			it('should bind result to body', () => {
				let ctx = { body : {} };
				let result = { test : true };
				setResponse(ctx, [], result);
				expect(ctx).to.deep.eql({
					body : {
						test : true
					}
				});
			});
		});
		describe('with single function argument', () => {
			it('should call the function with context and result', () => {
				let ctx = { body : {} };
				let result = { test : true };
				setResponse(ctx, [(ctx, result) => ctx.body = result], result);
				expect(ctx).to.deep.eql({
					body : {
						test : true
					}
				});
			});
		});
		describe('with single string argument', () => {
			it('should bind result to destination described by the string', () => {
				let ctx = { body : {} };
				let result = { test : true };
				setResponse(ctx, ['testdeep'], result);
				expect(ctx).to.deep.eql({
					body : {
						testdeep : {
							test : true
						}
					}
				});
			});
		});
		describe('with two arguments', () => {
			it('should bind specific value in result to destination', () => {
				let ctx = { body : {} };
				let result = { test : true };
				setResponse(ctx, ['test', 'testdeep'], result);
				expect(ctx).to.deep.eql({
					body : {
						testdeep : true
					}
				});
			});
		});
	});
});
