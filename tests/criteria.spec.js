import { expect } from 'chai';

import { Criteria, checkCriteria } from '../src/criteria';
import { id } from '../src/constants';

describe('Criteria decorator', () => {
	it('should expose Criteria', () => {
		expect(Criteria).to.be.a('function');
	});
	describe('Criteria', () => {
		let routes = [];
		let CriteriaDecorator = Criteria(routes);
		let controller = function() {};
		CriteriaDecorator('testing')({ [id] : 'id' }, 'key', { value : controller });
		it('should take routes and return a function that forward criteria description to route', () => {
			expect(CriteriaDecorator).to.be.a('function');
			expect(routes).to.deep.eql([{
				context : { [id] : 'id' },
				key : 'key',
				target : 'id',
				controller,
				criteria : [['testing']]
			}]);
		});
	});
	describe('checkCriteria', () => {
		let criteriaA = '/stringCriteria';
		let criteriaB = /Criteria/ig;
		let criteriaC = function FunctionCriteria(ctx) {
			return ctx.headers.testHeader == 'functionCriteria';
		};
		let arrayCriteria = [criteriaA, criteriaC];
		let orCriteria = {
			$or : [criteriaA, criteriaC]
		};
		describe('with string of criteria', () => {
			it('should match the path with string specified', () => {
				let ctx = { path : '/stringCriteria' };
				expect(checkCriteria(ctx, criteriaA)).to.eql(true);
			});
		});
		describe('with regexp of criteria', () => {
			it('should match the path with regexp specified', () => {
				let ctx = { path : '/regCriteria' };
				expect(checkCriteria(ctx, criteriaB)).to.eql(true);
			});
		});
		describe('with function of criteria', () => {
			it('should check request with the function', () => {
				let ctx = { headers : { testHeader : 'functionCriteria' } };
				expect(checkCriteria(ctx, criteriaC)).to.eql(true);
			});
		});
		describe('with array of criteria', () => {
			it('should check each criteria if ALL pass', () => {
				let ctx = { headers : { testHeader : 'functionCriteria' } };
				expect(checkCriteria(ctx, arrayCriteria)).to.eql(false);
			});
		});
		describe('with $or of criteria', () => {
			it('should check each criteria if SOME pass', () => {
				let ctx = { path : '/stringCriteria' };
				expect(checkCriteria(ctx, orCriteria)).to.eql(true);
			});
		});
	});
});
