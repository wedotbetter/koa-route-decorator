#!/bin/bash

git checkout release
git merge master -m "[MERGE] Merge from master for release"
npm run build
git add .

if [ "$1" = "patch" ] || [ "$1" = "minor" ] || [ "$1" = "major" ];
	then
	releasetype=$1
	else
	releasetype="patch"
fi

git commit -m "[RELEASE] $releasetype release"
npm version $releasetype

git push origin release --tags
git checkout master
git merge release -m "[MERGE] Merge release version"
git push
npm publish